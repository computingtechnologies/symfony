<?php


namespace ctblue\symfony\Controllers;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class ApiController extends AbstractController
{
    protected function serializeToJson($object)
    {
        $serializer = $this->container->get('serializer');
        $output = $serializer->serialize($object, 'json');
        return $output;
    }

    protected function renderAsJson($object, $statusCode = 200)
    {
        $jsonObject = $this->serializeToJson($object);

        $response = new JsonResponse();
        $response->setContent($jsonObject);
        $response->setStatusCode($statusCode);

        return $response;
    }
}