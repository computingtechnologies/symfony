<?php


namespace ctblue\symfony\Services;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;

class RepositoryService extends ServiceEntityRepository
{
    protected $entityClass;
    protected $registry;

    public function __construct(ManagerRegistry $registry, string $entityClass)
    {
        $this->registry = $registry;
        $this->entityClass = $entityClass;
        parent::__construct($registry, $entityClass);
    }

    /**
     * @param $sql
     * @param $params []|null
     * @return \Doctrine\DBAL\Result
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function execute($sql, $params = null)
    {
        if (!$params) $params = [];
        $query = $this->getEntityManager()->getConnection()->prepare($sql);
        $result = $query->executeStatement($params);
        return $result;
    }

    public function findBySql($sql, $params = null)
    {
        $rsm = new ResultSetMappingBuilder($this->getEntityManager());
        $rsm->addRootEntityFromClassMetadata($this->entityClass, $this->_entityName);
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        if ($params) {
            foreach ($params as $key => $value) {
                $query->setParameter($key, $value);
            }
        }
        return $query->getResult();
    }

    public function findArrayBySql($sql, $params = null)
    {
        $connection = $this->getEntityManager()->getConnection();
        if ($result = $connection->executeQuery($sql, $params)) {
            $results = $result->fetchAllAssociative();
            return $results;
        }
        return false;
    }
}