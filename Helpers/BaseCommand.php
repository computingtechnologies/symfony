<?php


namespace ctblue\symfony\Helpers;


use ctblue\symfony\Application;
use Psr\Container\ContainerInterface;
use Symfony\Component\Console\Command\Command;

class BaseCommand extends Command
{
    protected static $defaultName = 'base:command';
    protected ContainerInterface $container;

    public function __construct(ContainerInterface $container, string $name = null)
    {
        $this->container = $container;
        Application::$entityManager = $container->get('doctrine')->getManager();
        Application::$db = $container->get('doctrine')->getConnection();
        parent::__construct($name);
    }
}
