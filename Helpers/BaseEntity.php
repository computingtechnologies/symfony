<?php


namespace ctblue\symfony\Helpers;

use ctblue\symfony\Application;
use Doctrine\ORM\Mapping as ORM;

abstract class BaseEntity
{
    public abstract function getId(): ?int;


    public function save()
    {
        $this->setModificationDate(new \DateTime('now'));
        if ($this->getId() > -1) {
            return $this->update();
        } else {
            $this->setCreationDate(new \DateTime('now'));
            return $this->insert();
        }
    }
    private function checkSetup(){
        if(!Application::$db){
            throw new \Exception("Application::db has not been set. Run Application::setup() first");
        }
        if(!Application::$entityManager){
            throw new \Exception("Application::entityManager has not been set. Run Application::setup() first");
        }
    }
    public function update()
    {
        $this->checkSetup();
        Application::$entityManager->persist($this);
        Application::$entityManager->flush();
        return $this->isPersisted();
    }

    private function isPersisted()
    {
        $isPersisted = \Doctrine\ORM\UnitOfWork::STATE_MANAGED === Application::$entityManager->getUnitOfWork()->getEntityState($this);
        return $isPersisted;
    }

    public function insert()
    {
        $this->checkSetup();
        Application::$entityManager->persist($this);
        Application::$entityManager->flush();
        if ($this->getId() > -1) return true;
        return false;
    }

    public function delete()
    {
        $this->checkSetup();
        Application::$entityManager->remove($this);
        Application::$entityManager->flush();
        return !$this->isPersisted();
    }
}
