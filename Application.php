<?php


namespace ctblue\symfony;


use App\Repository\UserPermissionRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManager;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class Application
{
    /**
     * @var SessionInterface
     */
    public static $session;
    /**
     * @var Connection
     */
    public static $db;
    /**
     * @var EntityManager
     */
    public static $entityManager;
    /**
     * @var UserPermissionRepository
     */
    public static $userPermissionRepository;

    public static function setup(ContainerInterface $container)
    {
        Application::$entityManager = $container->get('doctrine')->getManager();
        Application::$db = $container->get('doctrine')->getConnection();
    }
}